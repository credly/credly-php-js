<?php

namespace Http;

class Badges extends Base {

	public function __construct()
	{

	}

	// public function getMembers(($request, $response, $args)
	// {

	// 	$curl = curl_init();

	// 	curl_setopt_array($curl, [
 //            CURLOPT_URL => 'https://api.credly.com/v1.1/members?' . http_build_query($_GET),
 //            CURLOPT_RETURNTRANSFER => true,
 //            CURLOPT_ENCODING => '',
 //            CURLOPT_MAXREDIRS => 10,
 //            CURLOPT_TIMEOUT => 30,
 //            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 //            CURLOPT_CUSTOMREQUEST => 'GET',
 //            CURLOPT_HTTPHEADER => [
 //                'x-api-key: 14198e4b879fcaad2ddb442a897565b8',
 //                'x-api-secret: maNzhEvivQULOJ+r1Yr/Hy8Z41SZz0Avn5M7VtUNPy/uwCsMjfDIyXhDeA2mN1HHeZCDrViZW4gk/            JzmYljwXPfCCB7FsKgFZnwDYbN0bNsPZObxx5GU61iBESK/nyA4sVRXaCwUMwQtu0O6r69b8wfhPQewfmE5zXPbjmhhc58='
 //                ],
 //            ]
	// 	);

	// 	$curlResponse = curl_exec($curl);
	// 	$err = curl_error($curl);

	// 	curl_close($curl);

	// 	$results = [];

	// 	if (! $err && is_string($curlResponse)) {
	// 		$jsonResults = json_decode($curlResponse);

	// 		if (json_last_error() === JSON_ERROR_NONE) {
	// 			$results = $jsonResults;
	// 		}
	// 	}

	// 	$response = $response
	// 		->withHeader('Content-Type', 'application/json')
	// 		->withJson($results);

	// 	return $response;
	// }

	public function getBadges($request, $response, $args)
	{

		$curl = curl_init();

		curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.credly.com/v1.1/badges?" . http_build_query($_GET),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'x-api-key: 14198e4b879fcaad2ddb442a897565b8',
                "x-api-secret: maNzhEvivQULOJ+r1Yr/Hy8Z41SZz0Avn5M7VtUNPy/uwCsMjfDIyXhDeA2mN1HHeZCDrViZW4gk/            JzmYljwXPfCCB7FsKgFZnwDYbN0bNsPZObxx5GU61iBESK/nyA4sVRXaCwUMwQtu0O6r69b8wfhPQewfmE5zXPbjmhhc58="
                ],
            ]
		);

		$curl_response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		$results = [];

		if (! $err && is_string($curl_response)) {
			$jsonResults = json_decode($curl_response);

			if (json_last_error() === JSON_ERROR_NONE) {
				$results = $jsonResults;
			}
		}

		$response = $response
			->withHeader('Content-Type', 'application/json')
			->withJson($results);

		return $response;
	}

}
