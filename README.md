# Credly Displayer

Simple SlimPHP/AngularJS app that lists the Badges from the Credly API.

## Running the server

php -S 0.0.0.0:8080 -t public public/index.php
